#include <string>
#include <iostream>
#include <stdio.h>  
#include <stdlib.h> 
#include <time.h>
#include <cctype>
#include <conio.h>
#include <vector>
#include "Pokemon.h"
using namespace std;

void main()
{
	srand(time(NULL));

	string playerName;
	bool intEnter = false;
	char npt[255];
	int choice;
	int wildNum;
	vector<Pokemon*> list;
	vector<Pokemon*> playerPokemon;

	//Pokemon List
	list.push_back(new Pokemon("Mewtwo", 106, 106, 1, 154, 0, 1, 10));
	list.push_back(new Pokemon("Dragonite", 91, 91, 1, 134, 0, 1, 10));
	list.push_back(new Pokemon("Haunter", 45, 45, 1, 115, 0, 1, 9));
	list.push_back(new Pokemon("Eevee", 55, 55, 1, 55, 0, 1, 8));
	list.push_back(new Pokemon("Gyarados", 95, 95, 1, 125, 0, 1, 10));
	list.push_back(new Pokemon("Jynx", 65, 65, 1, 115, 0, 1, 8));
	list.push_back(new Pokemon("Caterpie", 45, 45, 1, 30, 0, 1, 8));
	list.push_back(new Pokemon("Geodude", 40, 40, 1, 80, 0, 1, 9));
	list.push_back(new Pokemon("Zubat", 40, 40, 1, 45, 0, 1, 8));
	list.push_back(new Pokemon("Spearow", 40, 40, 1, 60, 0, 1, 8));
	list.push_back(new Pokemon("Rattata", 30, 30, 1, 56, 0, 1, 8));
	list.push_back(new Pokemon("Pikachu", 35, 35, 1, 55, 0, 1, 8));
	list.push_back(new Pokemon("Squirtle", 44, 44, 1, 50, 0, 1, 9));
	list.push_back(new Pokemon("Charmander", 39, 39, 1, 60, 0, 1, 9));
	list.push_back(new Pokemon("Bulbasaur", 45, 45, 1, 65, 0, 1, 9));

	// Intro
	cout << "Hello there! Welcome to the world of Pokemon." << endl;
	cout << endl;
	system("pause");
	system("cls");

	cout << "My name is Oak, but people call me the Pokemon Professor." << endl;
	cout << endl;
	system("pause");
	system("cls");

	cout << "This world is inhabited by creatures called Pokemon." << endl;
	cout << endl;
	system("pause");
	system("cls");

	cout << "For some people, Pokemon are pets. While others use them for battle." << endl;
	cout << endl;
	system("pause");
	system("cls");

	cout << "But for me..." << endl;
	cout << endl;
	system("pause");
	system("cls");

	cout << "I study Pokemon as a profession." << endl;
	cout << endl;
	system("pause");
	system("cls");

	cout << "First, what is your name?" << endl;
	cin >> playerName;
	system("cls");

	cout << "Right! So your name is " << playerName << "." << endl;
	cout << endl;
	system("pause");
	system("cls");

	cout << "Now, I have 3 unique Pokemon that I can give to you to start your journey with. Please choose one of your choice." << endl;
	cout << endl;
	system("pause");
	system("cls");

	do
	{
		system("cls");
		cout << "[1] Bulbasaur" << endl;
		cout << "[2] Charmander" << endl;
		cout << "[3] Squirtle" << endl;
		cin >> npt;
		string s = npt;
		choice = atoi(s.c_str());

		if (choice == 0)
		{
			continue;
		}
		else if (choice == 1)
		{
			playerPokemon.push_back(new Pokemon("Bulbasaur", 45, 45, 1, 65, 0, 1, 9));
			intEnter = true;
		}
		else if (choice == 2)
		{
			playerPokemon.push_back(new Pokemon("Charmander", 39, 39, 1, 60, 0, 1, 9));
			intEnter = true;
		}
		else if (choice == 3)
		{
			playerPokemon.push_back(new Pokemon("Squirtle", 44, 44, 1, 50, 0, 1, 9));
			intEnter = true;
		}
		else
		{
			continue;
		}
	} while (!intEnter);

	system("cls");
	cout << playerName << "! Your very own Pokemon legend is about to unfold." << endl;
	cout << endl;
	system("pause");
	system("cls");

	cout << "A world of dreams and adventures with Pokemon awaits. Let's go!" << endl;
	cout << endl;
	system("pause");
	system("cls");

	do
	{
		Pokemon* wildPokemon = list[wildNum];
		Pokemon* myPokemon = playerPokemon[0];

		cout << "A wild " << wildPokemon->name << " appeared!" << endl;
		cout << endl;
		system("pause");
		system("cls");

		cout << "Go! " << myPokemon->name << "!" << endl;
		cout << endl;
		system("pause");
		system("cls");

	} while (true);
}