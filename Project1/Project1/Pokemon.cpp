#include "Pokemon.h"

Pokemon::Pokemon(string name, int baseHP, int hp, int level, int baseDamage, int xp, int xpMod, int xpToLvlUp)
{
	this->name = name;
	this->baseHP = baseHP;
	this->hp = hp;
	this->level = level;
	this->baseDamage = baseDamage;
	this->xp = xp;
	this->xpMod = xpMod;
	this->xpToLvlUp = xpToLvlUp;
}

void Pokemon::displayStats()
{
	cout << "Name: " << this->name << endl;
	cout << "HP: " << this->hp << "/" << this->baseHP << endl;
	cout << "Level: " << this->level << endl;
	cout << "XP Until Next Level: " << this->xpToLvlUp << endl;
	cout << endl;
}

void Pokemon::attack(Pokemon * wildPokemon)
{
	cout << this->name << " attacks " << wildPokemon->name << "." << endl;
	cout << endl;
	wildPokemon->hp -= this->baseDamage;
	cout << wildPokemon->name << " takes the hit! Its current HP is now at: " << wildPokemon->hp << "/" << wildPokemon->baseHP << endl;
}

void Pokemon::lvlUp()
{
	cout << this->name << "has leveled up!" << endl;
	cout << endl;
	system("pause");
	system("cls");

	this->hp = hp + (hp * 0.15);
	this->baseHP = baseHP + (baseHP * 0.15);
	this->baseDamage = baseDamage + (baseDamage * 0.10);
	this->xp = 0;
	this->xpToLvlUp = xpToLvlUp + (xpToLvlUp * 0.20);
}