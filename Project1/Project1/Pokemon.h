#pragma once
#include <string>
#include <iostream>
using namespace std;

class Pokemon
{
public:
	Pokemon(string name, int baseHP, int hp, int level, int baseDamage, int xp, int xpMod, int xpToLvlUp);
	string name;
	int baseHP;
	int hp;
	int level;
	int baseDamage;
	int xp;
	int xpMod;
	int xpToLvlUp;
	void displayStats();
	void attack(Pokemon* wildPokemon);
	void lvlUp();
};
